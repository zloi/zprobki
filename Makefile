CC=gcc
ZNAME=zprobki
OBJECTS=zprobki.o
CLEAN=rm -f

$(ZNAME): $(OBJECTS)
	gcc -o $@ $^ `pkg-config --libs --cflags gtk+-2.0 gthread-2.0 libxml-2.0`
	$(CLEAN) *.o

zprobki.o: src/zprobki.c
	gcc -c $^  `pkg-config --libs --cflags gtk+-2.0 gthread-2.0 libxml-2.0` 

clean:
	$(CLEAN) *.o $(ZNAME)

