#include "zprobki.h"

int 
main (int argc, char ** argv)
{
    //init threads
    g_thread_init (NULL);
    gdk_threads_init ();

    //init gtk
    gtk_init (&argc, &argv);

    //load_config
    if (load_config () == 0 ) {
        fprintf (stderr, "Cannot load config file\n");
        return 0;
    }
    
    //load status icon
    if (status_icon_load () == 0) {
        fprintf (stderr, "Error loading status icon\n");
        return 1;
    }

    //load signal handler
    if (init_signal_handlers () == 0) {
        fprintf (stderr, "Cannot init signal handlers\n");
        return 1;
    }

    //start gtk_main
    gdk_threads_enter ();
    gtk_main ();
    gdk_threads_leave ();


    return 0;
}

int
status_icon_load ()
{
    status_icon = gtk_status_icon_new_from_file ( ZPROBKI_GREY );
    if (status_icon == NULL) {
        return 0;
    }
    
    gtk_status_icon_set_visible (status_icon, TRUE);
    
    //connect signals
    g_signal_connect (G_OBJECT ( status_icon ), "activate", G_CALLBACK ( status_icon_activate ), NULL);
    g_signal_connect (G_OBJECT ( status_icon ), "popup-menu", G_CALLBACK ( status_icon_popup ), NULL);

    return 1;    
}

void
status_icon_activate ()
{
    //do something on status icon left click
}

void 
status_icon_popup (GtkStatusIcon *_status_icon, guint button, 
    guint activate_time, gpointer user_data)
{
    GtkWidget   *menu;
    GtkWidget   *submenu;
    GtkWidget   *menuitem;
    GtkWidget   *submenuitem;
    
    zroute_t    *current_zroute = NULL;
    
    //create new menu
    menu = gtk_menu_new ();

    if (zroutes != NULL) {
        //add menu item for routes submenus
        menuitem = gtk_menu_item_new_with_label ("Routes");
        
        //create submenu for routes
        submenu = gtk_menu_new ();
        
        //create submenuitems for submenu
        current_zroute = zroutes;
        while (current_zroute != NULL) {
            submenuitem = gtk_menu_item_new_with_label (current_zroute->route_name);
            g_signal_connect (G_OBJECT ( submenuitem ), "activate", G_CALLBACK ( menu_route_activate ), (gpointer) current_zroute);
            gtk_menu_shell_append (GTK_MENU_SHELL ( submenu ), submenuitem);
        
            current_zroute = current_zroute->next;
        }
        
        //set submenu for item Routes
        gtk_menu_item_set_submenu (GTK_MENU_ITEM ( menuitem ), submenu);
        
        //add menuitem to main menu
        gtk_menu_shell_append (GTK_MENU_SHELL ( menu ), menuitem);
        
        //add separator
        menuitem = gtk_separator_menu_item_new ();
        gtk_menu_shell_append (GTK_MENU_SHELL ( menu ), menuitem);
    }
    
    //add item Settings
    menuitem = gtk_menu_item_new_with_label ("Settings");
    g_signal_connect (G_OBJECT ( menuitem ), "activate", G_CALLBACK ( menu_item_settings ), NULL);
    gtk_menu_shell_append (GTK_MENU_SHELL ( menu ), menuitem);

    //add item Update
    menuitem = gtk_menu_item_new_with_label ("Update");
    g_signal_connect (G_OBJECT ( menuitem ), "activate", G_CALLBACK ( menu_item_update ), NULL);
    gtk_menu_shell_append (GTK_MENU_SHELL ( menu ), menuitem);

    //add item Quit
    menuitem = gtk_menu_item_new_with_label ("Quit");
    g_signal_connect (G_OBJECT ( menuitem ), "activate", G_CALLBACK ( zprobki_quit ), NULL);
    gtk_menu_shell_append (GTK_MENU_SHELL ( menu ), menuitem);    
    
    gtk_widget_show_all (menu);

    gtk_menu_popup (GTK_MENU ( menu ), NULL, NULL, NULL, user_data, button, activate_time);
}

void 
menu_route_activate (GtkMenuItem *menuitem, gpointer user_data)
{ 
    zroute_t* current_zroute = (zroute_t*) user_data;
    
    //show or hide route window
    route_window_show_hide (current_zroute);    
}

void
route_window_show_hide (zroute_t *current_zroute)
{
    if (current_zroute->is_route_window_shown == 0) { //let show window
        if (!GTK_IS_WIDGET ( current_zroute->route_window )) { //route window isn't inited
            load_route_window (current_zroute);
        }
        else {
            gtk_widget_show_all (GTK_WIDGET ( current_zroute->route_window ));
            current_zroute->is_route_window_shown = 1;
        }
        gtk_window_move (current_zroute->route_window, current_zroute->route_window_x, current_zroute->route_window_y);
    }
    else { //let hide window
        if (GTK_IS_WIDGET ( current_zroute->route_window )) {
            gtk_window_get_position (current_zroute->route_window, &current_zroute->route_window_x, &current_zroute->route_window_y);
            gtk_widget_hide_all (GTK_WIDGET ( current_zroute->route_window ));
        }
        current_zroute->is_route_window_shown = 0;
    }
}

void 
menu_item_update ()
{
    kill (getpid (), SIGALRM);
}

int
load_settings_window ()
{
    GtkBuilder  *builder;
    GError      *error = NULL;
        
    //new builder
    builder = gtk_builder_new ();
    
    //try to load
    if (!gtk_builder_add_from_file ( builder, ZPROBKI_SETTINGS_WDGT, &error )) {
        gtk_message_dialog_new (NULL, 0, GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, "Error loading settings widget: %s\n", error->message);
        g_clear_error (&error);
        g_object_unref (G_OBJECT ( builder ));
        return 1;
    }
    
    //init settings window
    settings_window = GTK_WINDOW (gtk_builder_get_object ( builder, "settings_window" ));
    gtk_window_set_icon_from_file (settings_window, ZPROBKI_GREEN, NULL);
    g_signal_connect (G_OBJECT (settings_window), "destroy", G_CALLBACK ( close_settings_window ), NULL);
    
    //init elements on window...
    //entries
    update_period_entry = GTK_ENTRY (gtk_builder_get_object ( builder, "update_period_entry" ));
    route_name_entry = GTK_ENTRY (gtk_builder_get_object ( builder, "route_name_entry" ));
    route_url_entry = GTK_ENTRY (gtk_builder_get_object ( builder, "route_url_entry" ));
   
    //buttons
    ok_settings_button = GTK_BUTTON (gtk_builder_get_object ( builder, "ok_button" ));
    g_signal_connect (G_OBJECT ( ok_settings_button ), "clicked", G_CALLBACK ( ok_settings_button_clicked ), NULL);
        
    cancel_settings_button = GTK_BUTTON (gtk_builder_get_object ( builder, "cancel_button" ));
    g_signal_connect (G_OBJECT ( cancel_settings_button ), "clicked", G_CALLBACK ( cancel_settings_button_clicked ), NULL);
    
    add_route_button = GTK_BUTTON (gtk_builder_get_object ( builder, "add_route_button" ));
    g_signal_connect (G_OBJECT ( add_route_button ), "clicked", G_CALLBACK ( add_route_button_clicked ), NULL);

    remove_route_button = GTK_BUTTON (gtk_builder_get_object ( builder, "remove_route_button" ));
    g_signal_connect (G_OBJECT ( remove_route_button ), "clicked", G_CALLBACK ( remove_route_button_clicked ), NULL);
    
    save_route_button = GTK_BUTTON (gtk_builder_get_object ( builder, "save_route_button" ));
    g_signal_connect (G_OBJECT ( save_route_button ), "clicked", G_CALLBACK ( save_route_button_clicked ), NULL);    
    
    //treeview
    routes_view = GTK_TREE_VIEW ( gtk_builder_get_object ( builder, "routes_view" ));
    routes_view_init ();
    routes_view_populate ();
    routes_view_selection = gtk_tree_view_get_selection (routes_view);
    g_signal_connect (G_OBJECT ( routes_view_selection ), "changed", G_CALLBACK ( routes_view_selection_changed ), NULL );
    
    g_object_unref (G_OBJECT ( builder ));
    
    return 0;
}

void 
load_route_window (zroute_t *current_zroute)
{
    GtkBuilder          *builder;
    GError              *error = NULL;
    
    builder = gtk_builder_new ();
    
    //try to load
    if (!gtk_builder_add_from_file ( builder, ZPROBKI_ROUTE_WDGT, &error )) {
        gtk_message_dialog_new (NULL, 0, GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, "Error loading route widget: %s\n", error->message);
        g_clear_error (&error);
        g_object_unref (G_OBJECT ( builder ));
        return;
    }
    
    current_zroute->route_window = GTK_WINDOW (gtk_builder_get_object ( builder, "route_window" ));
    gtk_window_set_icon_from_file (current_zroute->route_window, ZPROBKI_GREEN, NULL);
    current_zroute->is_route_window_shown = 1;
    g_signal_connect (G_OBJECT ( current_zroute->route_window ), "configure-event", G_CALLBACK ( route_window_moved ), ( gpointer ) current_zroute);
    g_signal_connect (G_OBJECT ( current_zroute->route_window ), "destroy", G_CALLBACK ( route_window_destroy ), ( gpointer ) current_zroute);
    
    current_zroute->route_image = GTK_IMAGE (gtk_builder_get_object ( builder, "route_image" ));
    current_zroute->route_label = GTK_LABEL (gtk_builder_get_object ( builder, "route_label" ));
    
    gtk_widget_show_all (GTK_WIDGET ( current_zroute->route_window ));
    gtk_window_move (current_zroute->route_window, current_zroute->route_window_x, current_zroute->route_window_y);
    
    g_object_unref (G_OBJECT ( builder ));
}

void
route_window_moved  (GtkWidget  *widget, GdkEventConfigure *event, gpointer user_data)
{
    zroute_t    *current_zroute;
    
    current_zroute = (zroute_t*) user_data;
    gtk_window_get_position (current_zroute->route_window, &current_zroute->route_window_x, &current_zroute->route_window_y);
}

void
route_window_destroy  (GtkObject *object, gpointer user_data)
{
    zroute_t    *current_zroute;
    
    current_zroute = (zroute_t*) user_data;
    
    current_zroute->is_route_window_shown = 0;
    gtk_window_get_position (current_zroute->route_window, &current_zroute->route_window_x, &current_zroute->route_window_y);
}

void 
menu_item_settings ()
{
    char        *buffer = (char*) malloc (ZPROBKI_BUFFER_SIZE);
    
    if (buffer == NULL) {
        gtk_message_dialog_new ( NULL, 0, GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, "Cannot allocate memory", NULL);
        return;
    }
    
    if (!GTK_IS_WIDGET ( settings_window )) {
        if (load_settings_window () != 0) {
            fprintf (stderr, "Cannot load settings window\n");
            return;
        }
    }
    
    if (!is_settings_window_shown) {    //let show window       
        //init elements...
        snprintf (buffer, ZPROBKI_BUFFER_SIZE, "%i", zupdate_period/60);
        gtk_entry_set_text (update_period_entry, buffer);  
    
        gtk_entry_set_text (route_name_entry, "");
        gtk_entry_set_text (route_url_entry, "");
        selected_zroute_id = 0;
        
        //show window
        gtk_widget_show_all (GTK_WIDGET ( settings_window ));  
        is_settings_window_shown = 1;    
    }  
    
    //deallocate memory
    if (buffer != NULL) free (buffer);
}

void 
ok_settings_button_clicked ()
{
    //save general settings
    zupdate_period = atoi(gtk_entry_get_text ( update_period_entry ))*60;
    if (zupdate_period == 0) zupdate_period = 300; //set default
    
    //close window
    close_settings_window ();
}

void 
cancel_settings_button_clicked ()
{
    close_settings_window ();
}

void 
add_route_button_clicked ()
{
    zroute_t    *new_zroute;
    
    //allocate memory
    new_zroute = (zroute_t*) malloc (sizeof ( zroute_t ));
    if (new_zroute == NULL) {
        fprintf (stderr, "Cannot allocate memory for new zroute\n");
        return;
    }
    
    snprintf (new_zroute->route_name, ZPROBKI_BUFFER_SIZE, "New route");
    snprintf (new_zroute->url, ZPROBKI_BUFFER_SIZE, "url");
    
    //add new zroute to list
    add_new_zroute (new_zroute);
    
    //redraw treeview
    routes_view_populate (); 
}

void 
remove_route_button_clicked ()
{
    if (selected_zroute_id != 0) {
        remove_zroute (selected_zroute_id);
        routes_view_populate ();
        
        clear_routes_elements ();
    }
}

void 
save_route_button_clicked ()
{
    zroute_t    *current_zroute;
    
    if (selected_zroute_id != 0) {
        current_zroute = find_zroute (selected_zroute_id);
        if (current_zroute != NULL) {
            snprintf (current_zroute->route_name, ZPROBKI_BUFFER_SIZE, "%s", gtk_entry_get_text ( route_name_entry ));
            snprintf (current_zroute->url, ZPROBKI_BUFFER_SIZE, "%s", gtk_entry_get_text ( route_url_entry ));
        }
        
        routes_view_populate ();
    }
}

void 
routes_view_init ()
{
    GtkCellRenderer     *cell;
    GtkTreeViewColumn   *column;
    GtkTreeModel        *model;
    GtkListStore        *store;

    //add 1st cell, icon
    cell = gtk_cell_renderer_pixbuf_new ();
    gtk_tree_view_insert_column_with_attributes (routes_view, -1, "Icon", cell, "pixbuf", ROUTES_VIEW_ICON, NULL);
    
    //add 2nd cell, route name
    cell = gtk_cell_renderer_text_new ();
    gtk_tree_view_insert_column_with_attributes (routes_view, -1, "Route", cell, "text", ROUTES_VIEW_NAME, NULL);
    
    //add 3rd cell, id
    cell = gtk_cell_renderer_text_new ();
    gtk_tree_view_insert_column_with_attributes (routes_view, -1, "Id", cell, "text", ROUTES_VIEW_ID, NULL);
    
    //hide columns
    column = gtk_tree_view_get_column (routes_view, ROUTES_VIEW_ID);
    gtk_tree_view_column_set_visible (column, FALSE);
    
    //create store & model
    store = gtk_list_store_new (ROUTES_VIEW_COLUMNS, GDK_TYPE_PIXBUF, G_TYPE_STRING, G_TYPE_INT);
    model = GTK_TREE_MODEL (store);
    
    gtk_tree_view_set_model (routes_view, model);
    
    g_object_unref (model);
}

void 
routes_view_populate ()
{
    GtkListStore    *store;
    GtkTreeIter     iter;
    
    zroute_t        *current_zroute;
    
    if (zroutes != NULL) { //let fill in treeview
        //get store
        store = GTK_LIST_STORE (gtk_tree_view_get_model ( routes_view ));
        gtk_list_store_clear (store);
        
        current_zroute = zroutes;
        while (current_zroute != NULL) {
            gtk_list_store_append (store, &iter);
            gtk_list_store_set (store, &iter, ROUTES_VIEW_ICON, create_pixbuf ( ZPROBKI_GREEN ),
                ROUTES_VIEW_NAME, current_zroute->route_name, ROUTES_VIEW_ID, current_zroute->id, -1);
        
            current_zroute = current_zroute->next;
        }
    }
}

void
routes_view_selection_changed ()
{
    GtkTreeIter         iter;
    GtkTreeModel        *model;
    GtkTreeSelection    *selection;
    
    zroute_t            *current_zroute;
    
    selection = gtk_tree_view_get_selection (routes_view);
    
    if (gtk_tree_selection_get_selected ( selection, &model, &iter )) {
        //get selected route's id
        gtk_tree_model_get (model, &iter, ROUTES_VIEW_ID, &selected_zroute_id, -1);
        
        //find route
        current_zroute = find_zroute (selected_zroute_id);
        if (current_zroute != NULL) {
            gtk_entry_set_text (route_name_entry, current_zroute->route_name);
            gtk_entry_set_text (route_url_entry, current_zroute->url);
        }
    }
}

void 
clear_routes_elements ()
{
    gtk_entry_set_text (route_name_entry, "");
    gtk_entry_set_text (route_url_entry, "");
    
    selected_zroute_id = 0;
}

void
close_settings_window ()
{
    is_settings_window_shown = 0;
    gtk_widget_hide_all (GTK_WIDGET ( settings_window ));
}

void
get_general_traffic ()
{    
    /*imitation of getting general information
    we need random values from 1 till 10*/
    srand (time ( NULL ));
    traffic_value = rand () % 10 + 1;

    //change status_icon
    switch (traffic_value) {
        case 1: gtk_status_icon_set_from_file (status_icon, ZPROBKI_1); break;
        case 2: gtk_status_icon_set_from_file (status_icon, ZPROBKI_2); break;
        case 3: gtk_status_icon_set_from_file (status_icon, ZPROBKI_3); break;
        case 4: gtk_status_icon_set_from_file (status_icon, ZPROBKI_4); break;
        case 5: gtk_status_icon_set_from_file (status_icon, ZPROBKI_5); break;;
        case 6: gtk_status_icon_set_from_file (status_icon, ZPROBKI_6); break;
        case 7: gtk_status_icon_set_from_file (status_icon, ZPROBKI_7); break;
        case 8: gtk_status_icon_set_from_file (status_icon, ZPROBKI_8); break;
        case 9: gtk_status_icon_set_from_file (status_icon, ZPROBKI_9); break;
        case 10: gtk_status_icon_set_from_file (status_icon, ZPROBKI_10); break;
        default: gtk_status_icon_set_from_file (status_icon, ZPROBKI_GREY);
    }  
}

void 
get_route_traffic ()
{
    char        *tooltip_text = (char*) malloc (ZPROBKI_BUFFER_SIZE);
    char        *buffer = (char*) malloc (ZPROBKI_BUFFER_SIZE);
    
    zroute_t    *current_zroute;
  
    srand (time ( NULL ));  //temporary
    
    snprintf (tooltip_text, ZPROBKI_BUFFER_SIZE, "Traffic jam - %i point(s)\n", traffic_value);
    
    if (zroutes != NULL) {
        current_zroute = zroutes;
        
        while (current_zroute != NULL) {
            /*imitation of getting info of each route, use random*/
            current_zroute->traffic_value = rand () % 10 + 1;
            
            strncpy (buffer, tooltip_text, ZPROBKI_BUFFER_SIZE);
            snprintf (tooltip_text, ZPROBKI_BUFFER_SIZE, "%s\n%s - %i point(s)", buffer, current_zroute->route_name, current_zroute->traffic_value);
            
            if (GTK_IS_WIDGET ( current_zroute->route_window )) {    
                switch (current_zroute->traffic_value) {
                    case 1: gtk_image_set_from_file (current_zroute->route_image, ZPROBKI_1); break;
                    case 2: gtk_image_set_from_file (current_zroute->route_image, ZPROBKI_2); break;
                    case 3: gtk_image_set_from_file (current_zroute->route_image, ZPROBKI_3); break;
                    case 4: gtk_image_set_from_file (current_zroute->route_image, ZPROBKI_4); break;
                    case 5: gtk_image_set_from_file (current_zroute->route_image, ZPROBKI_5); break;
                    case 6: gtk_image_set_from_file (current_zroute->route_image, ZPROBKI_6); break;
                    case 7: gtk_image_set_from_file (current_zroute->route_image, ZPROBKI_7); break;
                    case 8: gtk_image_set_from_file (current_zroute->route_image, ZPROBKI_8); break;
                    case 9: gtk_image_set_from_file (current_zroute->route_image, ZPROBKI_9); break;
                    case 10: gtk_image_set_from_file (current_zroute->route_image, ZPROBKI_10); break;
                    default: gtk_image_set_from_file (current_zroute->route_image, ZPROBKI_GREY);               
                }
                
                snprintf (buffer, ZPROBKI_BUFFER_SIZE, "%s - %i point(s)", current_zroute->route_name, current_zroute->traffic_value);
                gtk_label_set_text (current_zroute->route_label, buffer);
            }
            
            current_zroute = current_zroute->next;
        }
    }
    
    gtk_status_icon_set_tooltip (status_icon, tooltip_text);
    
    if (tooltip_text != NULL) free (tooltip_text);
    if (buffer != NULL) free (buffer);
}

void
zprobki_quit ()
{
    //save variables and preferences
    if (save_config () == 0) {
        fprintf (stderr, "Error during saving config file\n");
    }

    remove_all_zroute ();
    //quit
    gtk_main_quit ();
}

int 
init_signal_handlers ()
{
    struct sigaction sig_alrm;

    //handle SIGALRM
    sigemptyset (&sig_alrm.sa_mask);
    sig_alrm.sa_handler = &sig_alrm_handler;
    sig_alrm.sa_flags = 0;

    if (sigaction ( SIGALRM, &sig_alrm, NULL ) == -1) {
        fprintf (stderr, "Error init handler for SIGALRM\n");
        return 0; 
    }

    kill( getpid (), SIGALRM);

    return 1;
}

void 
sig_alrm_handler (int snum)
{
    get_general_traffic ();
    get_route_traffic ();
    
    alarm (zupdate_period);
}

int 
load_config ()
{
    xmlDocPtr   doc;
    xmlNodePtr  cur;
    
    xmlChar     *update_period;
    xmlChar     *route_name;
    xmlChar     *route_url;
    xmlChar     *route_window_x;
    xmlChar     *route_window_y;
    
    zroute_t    *new_zroute;
    
    //check config
    doc = xmlParseFile (ZPROBKI_CONFIG);
    if (doc == NULL) {
        fprintf (stderr, "Cannot parse config file\n");
        return 0;
    }
    
    cur = xmlDocGetRootElement (doc);
    if (cur == NULL) {
        fprintf (stderr, "Empty config file\n");
        xmlFreeDoc (doc);
        return 0;
    }
    
    if (xmlStrcmp ( cur->name, (const xmlChar*) "zprobki" )) {
        fprintf (stderr, "Invalid config file\n");
        xmlFreeDoc (doc);
        return 0;
    } 
    
    cur = cur->xmlChildrenNode;
    while (cur != NULL) {
        //update period
        if (( !xmlStrcmp (cur->name, ( const xmlChar* ) "update_period") )) {
            update_period = xmlGetProp (cur, ( const xmlChar* ) "value");
            zupdate_period = atoi (( const char* ) update_period);
            if (zupdate_period == 0) zupdate_period = 300;
            xmlFree (update_period);
        }
        //route
        if (( !xmlStrcmp (cur->name, ( const xmlChar* ) "route") )) {
            route_name = xmlGetProp (cur, ( const xmlChar* ) "route_name");
            route_url = xmlGetProp (cur, ( const xmlChar* ) "route_url");
            route_window_x = xmlGetProp (cur, ( const xmlChar* ) "window_x");
            route_window_y = xmlGetProp (cur, ( const xmlChar* ) "window_y");
            
            new_zroute = (zroute_t*) malloc (sizeof ( zroute_t ));
            if (new_zroute == NULL) {
                fprintf (stderr, "Cannot allocate memory for new route\n");
            }
            else {
                snprintf (new_zroute->route_name, ZPROBKI_BUFFER_SIZE, "%s", ( const char* ) route_name);
                snprintf (new_zroute->url, ZPROBKI_BUFFER_SIZE, "%s", ( const char* ) route_url);
                new_zroute->route_window_x = atoi (( const char* ) route_window_x);
                new_zroute->route_window_y = atoi (( const char* ) route_window_y);
                
                add_new_zroute (new_zroute);
                load_route_window (new_zroute);
            }
            
            xmlFree (route_name);
            xmlFree (route_url);
            xmlFree (route_window_x);
            xmlFree (route_window_y);
        }
        cur = cur->next;
    }
    
    xmlFreeDoc (doc);
    return 1;
}

int 
save_config ()
{
    FILE        *fconfig;
    char        *buffer;
    zroute_t    *current_zroute;

    //open config files
    fconfig = fopen (ZPROBKI_CONFIG, "w");
    if (fconfig == NULL) {
        fprintf (stderr, "Cannot open config file\n");
        return 0;
    }
    
    //allocate memory
    buffer = (char*) malloc (ZPROBKI_BUFFER_SIZE);
    if (buffer == NULL) {
        fprintf (stderr, "Cannot allocate memory for buffer\n");
        
        if (fconfig != NULL) fclose (fconfig);
        return 0;
    }

    //write header
    fputs ("<zprobki>\n", fconfig);
    
    //zupdate_period
    snprintf (buffer, ZPROBKI_BUFFER_SIZE, "\t<update_period value=\"%d\" />\n", zupdate_period);
    fputs (buffer, fconfig);

    //routes
    if (zroutes != NULL) {
        current_zroute = zroutes;
        while (current_zroute != NULL) {
            snprintf (buffer, ZPROBKI_BUFFER_SIZE, "\t<route route_name=\"%s\" route_url=\"%s\" window_x=\"%i\" window_y=\"%i\"/>\n", 
                current_zroute->route_name, current_zroute->url, current_zroute->route_window_x, current_zroute->route_window_y);
            fputs (buffer, fconfig);
                
            current_zroute = current_zroute->next;
        }
    }

    //write footer
    fputs ("</zprobki>", fconfig);
    
    //close config file
    if (fconfig != NULL) fclose (fconfig);

    //deallocate memory
    if (buffer != NULL) free (buffer);
}

void 
add_new_zroute (zroute_t *new_zroute)
{
    static int id = 0;
    zroute_t *current_zroute = NULL;

    id++;
    new_zroute->id = id;
    new_zroute->next = NULL;

    if (zroutes == NULL) { 
        zroutes = new_zroute;
    }
    else {
        current_zroute = zroutes;
        while (current_zroute->next != NULL) {
            current_zroute = current_zroute->next;
        }
        current_zroute->next = new_zroute;
    }
}

void 
remove_zroute (int id)
{
    zroute_t *current_zroute = NULL;
    zroute_t *before_zroute = NULL;

    if (zroutes != NULL) {
        current_zroute = zroutes;

        while (current_zroute != NULL) {
            if (current_zroute->id == id) {
                break;
            }
            else {
                before_zroute = current_zroute;
                current_zroute = current_zroute->next;
            }
        }
        
        if (current_zroute != NULL) {
            if (before_zroute == NULL) { //first
                zroutes = current_zroute->next;
            }
            else {
                before_zroute->next = current_zroute->next;
            }
            if (current_zroute != NULL) free (current_zroute);
        }
    }
}


zroute_t* find_zroute (int id)
{
    int is_found = 0;
    zroute_t *current_zroute = NULL;

    if (zroutes == NULL) 
        return NULL;
    else {
        current_zroute = zroutes;
        
        while (current_zroute != NULL) {
            if (current_zroute->id == id) { 
                is_found = 1;
                break;
            }
            
            current_zroute = current_zroute->next;
        }            
        
        if (is_found) 
            return current_zroute;
        else 
            return NULL;
    }
}

void 
remove_all_zroute ()
{
    zroute_t *current_zroute = NULL;
    zroute_t *next_zroute = NULL;
    
    if (zroutes != NULL) {
        current_zroute = zroutes;
        
        while (current_zroute->next != NULL) {
            next_zroute = current_zroute->next;
            if (current_zroute != NULL) free (current_zroute);
            current_zroute = next_zroute;
        }
        
        if (current_zroute != NULL) 
            free (current_zroute);
    }
}

GdkPixbuf*
create_pixbuf (const char* filename)
{
    GdkPixbuf   *pixbuf;
    GError      *error = NULL;
    
    pixbuf = gdk_pixbuf_new_from_file (filename, &error);
    if (!pixbuf) {
        fprintf (stderr, "Error creating pixbuf (%s)\n", filename);
        g_clear_error (&error);
    }   
    
    return pixbuf;
}
