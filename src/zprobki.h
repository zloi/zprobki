#ifndef ZPROBKI_H
#define ZPROBKI_H

#include <stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <curl/curl.h>
#include <signal.h>
#include <string.h>
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <time.h>   //temporary header for srand

//constants & defines
#define ZPROBKI_BUFFER_SIZE 4096

#define ZPROBKI_SETTINGS_WDGT "./widget/settings.glade"
#define ZPROBKI_ROUTE_WDGT "./widget/route.glade"
#define ZPROBKI_CONFIG "./zprobki.xml"

#define ZPROBKI_GREY "icon/grey.png"
#define ZPROBKI_GREEN "icon/green.png"
#define ZPROBKI_YELLOW "icon/yellow.png"
#define ZPROBKI_RED "icon/red.png"

#define ZPROBKI_1 "icon/1.png"
#define ZPROBKI_2 "icon/2.png"
#define ZPROBKI_3 "icon/3.png"
#define ZPROBKI_4 "icon/4.png"
#define ZPROBKI_5 "icon/5.png"
#define ZPROBKI_6 "icon/6.png"
#define ZPROBKI_7 "icon/7.png"
#define ZPROBKI_8 "icon/8.png"
#define ZPROBKI_9 "icon/9.png"
#define ZPROBKI_10 "icon/10.png"

//typedefs and others...
typedef struct zroute_s {
    struct zroute_s* next;

    int             id;                                 //id
    int             traffic_value;                      //traffic value
    
    char            route_name [ZPROBKI_BUFFER_SIZE];   //route name
    char            url [ZPROBKI_BUFFER_SIZE];          //url
    char            route_time [ZPROBKI_BUFFER_SIZE];   //time for getting route, not used yet
    
    GtkWindow       *route_window;                      //pointer to route window
    int             route_window_x;                     //position x
    int             route_window_y;                     //position y
    int             is_route_window_shown;              //if route window is shown or not
    
    GtkImage        *route_image;                       //pointer to route image
    GtkLabel        *route_label;                       //pointer to route label
} zroute_t;

enum {
    ROUTES_VIEW_ICON = 0,
    ROUTES_VIEW_NAME,
    ROUTES_VIEW_ID,
    ROUTES_VIEW_COLUMNS
};

//variables..
zroute_t    *zroutes = NULL;                        //list of routes
int         zupdate_period = 300;                   //update period in seconds
int         traffic_value = 0;

GtkStatusIcon    *status_icon;                      //status icon on the panel

/* settings window */
GtkWindow        *settings_window;                  //settings window

GtkTreeView      *routes_view;                      //tree view with routes
GtkTreeSelection *routes_view_selection;            //selection object for tree view

GtkEntry         *update_period_entry;              //entry update period
GtkEntry         *route_name_entry;                 //entry route name
GtkEntry         *route_url_entry;                  //entry url 

GtkButton        *ok_settings_button;               //pointer to ok button
GtkButton        *cancel_settings_button;           //pointer to cancel button
GtkButton        *add_route_button;                 //pointer to add button
GtkButton        *remove_route_button;              //pointer to remove button
GtkButton        *save_route_button;                //pointer to save button

int              is_settings_window_shown = 0;      //if settings window is shown or not
int              selected_zroute_id = 0;            //current selected route in tree view
   
//functions

/*  int status_icon_load ();
    Function loads icon in panel and connect
    signals for this icon. Function returns 1 if success,
    0 if some error
*/
int status_icon_load ();


/*  void status_icon_activate ();
    Function calls something on left click by icon :)
*/
void status_icon_activate ();


/*  void status_icon_popup (...);
    Function shows menu and connects singnals for menu items
*/
void status_icon_popup (GtkStatusIcon *_status_icon, 
    guint button, guint activate_time, gpointer user_data);


/*  void menu_route_activate (...);
    Function is called when user click on route menu
*/
void menu_route_activate (GtkMenuItem *menuitem, gpointer user_data);


/*  void route_window_show_hide (...);
    Function shows or hide route window
*/
void route_window_show_hide (zroute_t *current_zroute);


/*  void menu_item_update ()
    Function is called when user click on Update menu
*/
void menu_item_update ();


/*  void load_settings_window ();
    Function loads settings window
*/
int load_settings_window ();


/*  void load_route_window (...);
    Function loads route window
*/
void load_route_window (zroute_t *current_zroute);


/*  void route_window_moved  (...);
    Function is called when route window has moved
*/
void route_window_moved (GtkWidget  *widget, GdkEventConfigure *event, gpointer user_data);


/*  void route_window_destroy (...)
    When route window has closed
*/
void route_window_destroy  (GtkObject *object, gpointer user_data);


/*  void menu_item_setting ()
    Function is called when user click on Settings menu
*/
void menu_item_settings ();


/*  void ok_settings_button_clicked ();
    Function saves settings
*/
void ok_settings_button_clicked ();


/*  void cancel_settings_button_clicked ();
    Just closes settings window
*/
void cancel_settings_button_clicked ();


/*  void add_route_button_clicked ();
    Adds a new route
*/
void add_route_button_clicked ();


/*  void remove_route_button_clicked ();
    Removes route 
*/
void remove_route_button_clicked ();


/*  void save_route_button_clicked ();
    Saves route changes
*/
void save_route_button_clicked ();


/*  void routes_view_init ();
    Inits route tree view
*/
void routes_view_init ();


/*  void routes_view_populate ();
    Fills in route tree view
*/
void routes_view_populate ();


/*  void routes_view_changed();
    Called when user click on route in tree view
*/
void routes_view_selection_changed();


/*  void clear_routes_elements ();
    clears entries in settings window
*/
void clear_routes_elements ();

/*  void close_settings_window ();
    Closes settings window
*/
void close_settings_window ();


/*  void get_general_traffic ();
    Function gets general information about traffic jams
*/
void get_general_traffic ();


/*  void get_destination_traffic ();
*/
void get_route_traffic ();


/*  void zprobki_quit ();  
    Fucntion does something before closing application and
    closes it
*/
void zprobki_quit ();


/*  int init_signal_handlers ();
    System signals handler
*/
int init_signal_handlers ();


/*  void sig_alrm_handler (...);
    Handler for SIGALRM
*/
void sig_alrm_handler (int snum);


/*  int load_config ();
    Loads config
*/
int load_config ();


/*  int save_config ();
    Saves config
*/
int save_config ();

/*  void add_new_zroute (...);
    Adds new route to list
*/
void add_new_zroute (zroute_t* new_zroute);


/*  void remove_zroute (...);
    Removes route from list
*/
void remove_zroute (int id);


/*  zroute_t* find_zroute (...);
    Finds route by id
*/
zroute_t* find_zroute (int id);


/*  void remove_all_zroute ();
    Removes all routes
*/
void remove_all_zroute ();


/*  GdkPixbuf* create_pixbuf (const char* filename);
*/
GdkPixbuf* create_pixbuf (const char* filename);
#endif
